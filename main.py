__author__ = "Tran Ba Quang"
__copyright__ = "Copyright 2020, AutoPostFacebook Project"
__credits__ = "Tran Ba Quang"
__contact__ = "https://facebook.com/quang.tranba.37"
__version__ = "2.0.0 beta"
__date__ = "22/05/2020"
__maintainer__ = "Tran Ba Quang"

'''
Version 2.0 beta
Thông tin:
    - Chuyển sang tài khoản khác nếu tài khoản hiện tại xảy ra lỗi (sai tên hoặc mật khẩu)
    - Sửa lỗi ConnectionResetError và HttpError
    - Tách nhỏ phương thức auto:
        cụ thể: +, autoOnAnAccount(): Tự động trên một tài khoản (thay vì chạy 1 loạt như trước, giúp xử lý lỗi ConnectionResetError thông minh hơn)
        đổi tên auto() => autoAll()
    - Thêm phương thức postToMultipleGroups() thực hiện đăng bài trên nhiều group dựa vào danh sách cho trước(lấy từ file Post/group.txt)
    - Thêm phương thức strip_emoij(): loại bỏ các kí tự lạ của content, gây lỗi cho hàm send_keys
'''

# Thư viện hỗ trợ
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import RemoteDriverServerException

# Thư viện có sẵn
from time import sleep
import os
import re

# Thư viện Customer
import printProcessBar as ProcessBar
from readConfig import account_list, group_list, file_list, content_text

EMAIL = 0
PASSWORD = 1

class AutoBanHang:
    def __init__(self):
        self.driver = ''
    
    # tạo ra một webdriver(chrome)
    def webdriver(self):
        chrome_options = webdriver.ChromeOptions();
        prefs = {
            "profile.managed_default_content_settings.images": 2,
        }
        chrome_options.add_argument('--log-level=3')
        chrome_options.add_experimental_option("prefs", prefs)
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-notifications')
        driver = webdriver.Chrome(executable_path='./chromedriver.exe', options=chrome_options)
        return driver

    # thông tin tài khoản, các tệp và các group chuẩn bị đăng
    def infoPost(self):
        print('\u2554\u2550\u2550{:\u2550<38}\u2557'.format(' Tài Khoản '))
        for account in account_list:  
            print('\u2551{:^40}\u2551'.format(account[0]))
        print('\u255A{:\u2550<40}\u255D'.format(''))
        print('\u2554{:\u2550^80}\u2557'.format(' Đăng các file sau '))
        for file in file_list:  
            print('\u2551{:<80}\u2551'.format(file))
        print('\u255A{:\u2550<80}\u255D'.format(''))
        print('\u2554{:\u2550>78}\u2550\u2550\u2557'.format(' Đăng lên các group sau '))
        for group in group_list:  
            print('\u2551{:<80}\u2551'.format(group))
        print('\u255A{:\u2550<80}\u255D'.format(''))

    # Đăng nhập vào facebook
    # email: tên đăng nhập
    # password: mật khẩu
    def login(self, email, password):
        try:
            self.driver.get("https://facebook.com")
            e_email = self.driver.find_element_by_xpath('//*[@id="email"]')
            e_password = self.driver.find_element_by_xpath('//*[@id="pass"]')
            e_email.send_keys(email)
            e_password.send_keys(password, Keys.RETURN)
            ready_state = ''
            while ready_state != 'complete':
                ready_state = self.driver.execute_script('return document.readyState')
                print(ready_state)
            url_present = self.driver.execute_script('return document.URL')
            print(url_present)
            if url_present == 'https://www.facebook.com/':
                sleep(3)
                url_present = self.driver.execute_script('return document.URL')
                print(url_present)
                if url_present == 'https://www.facebook.com/':
                    return True
                return False
            return False
        except Exception as err:
            print('\r\033[01;31mERROR: ', err, '\033[0m')
            return 'ConnectionResetError'
        

    # chờ tới khi phần tử Xpath xuất hiện
    # timeWait: (int) thời gian chờ 
    # xpath: phần tử tìm kiếm dạng XPATH
    def waitXpathElementPresence(self, timeWait, xpath):
        return WebDriverWait(self.driver, timeWait).until(EC.presence_of_element_located((By.XPATH, xpath)))
    
    # chờ tới khi phần tử CssSelector xuất hiện
    # timeWait: (int) thời gian chờ 
    # selector_css: phần tử tìm kiếm dạng CSS_SELECTOR
    def waitSelectorCssElementPresence(self, timeWait, selector_css):
        return WebDriverWait(self.driver, timeWait).until(EC.presence_of_element_located((By.CSS_SELECTOR, selector_css)))
    
    def isElementActive(self, times, timeWait, element):
        times = times
        element_active = False
        while not element_active and times != 0:
            try:
                element = WebDriverWait(self.driver, timeWait).until(EC.presence_of_element_located(element))
                element_active = True
                return element
            except Exception as e:
                # print(e)
                print('\r\u21BA  \033[31mKhông tìm thấy phần tử, đang thử lại (\033[01;31m{}\033[00;31m)\033[0m'.format(times), end='')
                element_active = False
                times -= 1
        return False

    # xóa phiên đăng nhập hiện tại
    def deleteCokies(self, email):
        self.driver.delete_all_cookies()
        self.driver.refresh()
        print('\033[2m Đã xóa phiên đăng nhập cho \033[3;4;2m{}\033[0m'.format(email))

    # tìm phần tử document.querrySelector('input[type="file"]') và thực hiện tải lên tệp
    # file: tệp muốn tải lên
    def findElementAndUploadFile(self, file, index):
        find_file = False
        while not find_file:
            try:
                sleep(1)
                post = self.waitXpathElementPresence(timeWait=10, xpath='//input[@type="file"]')
                # print(post.get_attribute('aria-label'))
                post.send_keys(file)
                find_file = True
                print('\r\u2794  \033[33mĐang tải lên: \033[34m{}\033[0m'.format(file), end='')
                print('\r\u2713  \033[35m[{:^3}] Đã tải lên:\033[0m {}'.format(index, file))
            except Exception:
                find_file = False
                print('\r\u21BA  \033[31mCó lỗi xảy ra khi tìm phần tử tải lên file, đang thử lại !\033[0m', end='')
        try:
            check_upload = False
            while not check_upload:
                status = self.waitSelectorCssElementPresence(timeWait=2, selector_css='._417d')
                status = status.get_attribute('innerHTML')
                x = re.search('aria-valuenow="(.*?)"', status)
                x = round(float(x.group(1)), 2)
                if x != 100 :
                    ProcessBar.printProcessBar(toast='\u2794  Đang tải file.........', process=x)
                else:
                    ProcessBar.printProcessBar(toast='\u2794  Đang tải file.........', process=x)
                    check_upload = True
        except Exception:
            pass

    # đăng lên trang cá nhân của tài khoản hiện tại
    # content: (file *txt) nội dụng của bài đăng (status) // nội dung được lấy từ file Post/content.txt
    # file_list: (file *txt) danh sách các file muốn đăng lên // danh sách được lấy theo dòng trong file Post/group.txt
    def postWall(self, content, file_list):
        print('==========ĐĂNG BÀI LÊN TRANG CÁ NHÂN============')
        xpath_post = "//*[@id='mount_0_0_3C']/div/div[1]/div/div[4]/div/div/div[1]/div/div[2]/div/div/div/form/div/div[1]/div/div/div/div[2]/div[1]/div[1]/div[1]/div/div/div/div/div[2]/div"
        # input_content = self.isElementActive(times=5, timeWait=20, element=(By.XPATH, '//textarea[@name="xhpc_message"]'))
        input_content = self.isElementActive(times=5, timeWait=20, element=(By.XPATH, xpath_post))
        if input_content:
            input_content.send_keys(self.strip_emoji(content))
            print('Nội dung đăng:\n{}'.format(self.strip_emoji(content)))
            for i, file in zip(range(len(file_list)), file_list):
                print('\r[{}]  Tải lên file: \033[33m{}\033[0m'.format((i + 1), file), end='')
                self.findElementAndUploadFile(file, (i+1))
            try:
                story = self.waitXpathElementPresence(timeWait=10, xpath='//li[@aria-disabled="false"]')
                story.click()
                print('\u2713  Đăng lên Story')
            except Exception:
                print('\u00d7  Không thể đăng lên Story')
            button_post_check = False
            while not button_post_check:
                try:
                    post = WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, '._1mf7._4r1q._4jy0._4jy3._4jy1._51sy.selected._42ft')))
                    post.click()
                    button_post_check = True
                except Exception:
                    # nếu không thể tìm thấy nút đăng bài sẽ kiểm trả xem nút "Đăng" có bị Disable không
                    button_post_check = False
                    print('Tìm nút post')
                    try:
                        noPost = self.waitSelectorCssElementPresence(timeWait=1, selector_css='._1pek._6_lt')
                        noPost.click()
                    except Exception:
                        # nếu không bị Disable, thì quay lại tìm nút "Đăng"
                        button_post_check = False
                        print('Không bị Disable')
            try:
                self.waitSelectorCssElementPresence(timeWait=60, selector_css='._2oj3')
                #print('Tìm xong process')
                check_post_ok = False
                while not check_post_ok:
                    try:
                        process = self.driver.find_element_by_css_selector('.__q7').get_attribute('style')
                        process = round(float(process[7:-2]), 2)
                        ProcessBar.printProcessBar(toast='\u2794  Đang tải lên.........', process=process)
                        self.driver.find_element_by_css_selector('._2oj3')
                    except Exception:
                        check_post_ok = True
                        print('\n\u2713  \033[32mĐăng lên thành công\033[0m')
            except Exception:
                pass

    # kiểm tra xem đã tham gia group chưa
        # return True: đã tham gia
        # return False: chưa tham gia
    # link_group: (string) đường dẫn (url) tới group
    def checkJoinGroup(self, link_group):
        sleep(0.5)
        self.driver.get(link_group)
        try:
            self.waitSelectorCssElementPresence(timeWait=3, selector_css='._42ft._4jy0._55pi._2agf._4o_4._p._4jy4._517h._51sy')
            '''Đã tham gia nhóm'''
            return True
        except Exception:
            '''Chưa tham gia nhóm'''
            return False
    
    # kiểm tra trạng thái của trang cá nhân với group (đã tham gia, đang chờ phê duyệt, . . .)
    # link_group: (string) đường dẫn (url) tới group
    def checkStatusJoin(self, link_group):
        if self.checkJoinGroup(link_group):
            check_join_group = self.waitSelectorCssElementPresence(timeWait=0, selector_css='._42ft._4jy0._55pi._2agf._4o_4._p._4jy4._517h._51sy')
            check_join_group = check_join_group.get_attribute('innerText')
            check_join_group = check_join_group.strip()
            print(check_join_group)
            return check_join_group
        else:
            print('\u26A0  Chưa tham gia group')
            join_group = self.waitSelectorCssElementPresence(timeWait=3,selector_css='._42ft._4jy0._21ku._4jy4._4jy1.selected._51sy.mrm')
            join_group.click()
            sleep(0.1)
            print('\u2611  Đã gửi yêu cầu tham gia group')
            self.driver.refresh()
            self.driver.refresh()
            try:
                self.waitSelectorCssElementPresence(timeWait=1, selector_css='._42ft._4jy0._21ku._4jy4._4jy1.selected._51sy.mrm')
                self.checkStatusJoin(link_group)
            except Exception:
                check_join_group = self.waitSelectorCssElementPresence(timeWait=0, selector_css='._42ft._4jy0._55pi._2agf._4o_4._p._4jy4._517h._51sy')
                check_join_group = check_join_group.get_attribute('innerText')
                check_join_group = check_join_group.strip()
                print('\u2794  {}'.format(check_join_group))
                return check_join_group

    # thực hiện đăng bài vào group của tài khoản hiện tại
    # content: (file *txt) nội dụng của bài đăng (status) // nội dung được lấy từ file Post/content.txt
    # link_group: (string) đường dẫn (url) tới group
    # file_list: (file *txt) danh sách các file muốn đăng lên // danh sách được lấy theo dòng trong file Post/group.txt
    def postGroup(self, content, link_group, file_list):
        if self.checkStatusJoin(link_group) == 'Đã tham gia':
            #input_content = self.waitXpathElementPresence(timeWait=120, xpath='//textarea[@name="xhpc_message_text"]')
            input_content = self.isElementActive(times=5, timeWait=20, element=(By.XPATH, '//textarea[@name="xhpc_message_text"]'))
            if input_content:
                input_content.send_keys(self.strip_emoji(content))
                for i, file in zip(range(len(file_list)), file_list):
                    print('\r[{}]  File: {}'.format((i + 1), file), end='')
                    self.findElementAndUploadFile(file, (i+1))
                check_button_post = False
                while not check_button_post:
                    try:
                        self.waitSelectorCssElementPresence(timeWait=0, selector_css='._1mf7._4jy0._4jy3._4jy1._51sy.selected._42ft._42fr')
                        check_button_post = False
                    except Exception:
                        try:
                            post = self.waitSelectorCssElementPresence(timeWait=10, selector_css='._1mf7._4jy0._4jy3._4jy1._51sy.selected._42ft')
                            post.click()
                            check_button_post = True
                            ok = False
                            while not ok:
                                try:
                                    self.waitSelectorCssElementPresence(timeWait=2, selector_css='._5gm9')
                                    ok = False
                                except Exception:
                                    ok = True
                            print('\u2713  \033[32mĐăng lên thành công\033[0m')
                        except Exception:
                            check_button_post = False
                            print('\r\u21BA  \033[31mCó lỗi xảy ra khi tìm phần tử tải lên file, đang thử lại !\033[0m')
        else:
            print('\033[31mChưa tham gia nên không thể đăng bài!\033[0m')
    
    # thực hiện đăng lên nhiều group dựa theo danh sách
    # groups: (list) danh sách url group cần đăng bài
    # time_between_two_posts: (int) thời gian nghỉ giữa 2 lần đăng bài lên group
    def postToMultipleGroups(self, groups, time_between_two_posts):
        for i, group in zip(range(len(groups)), groups):
            print('Group: {}'.format(group))
            post_group = False
            # số lần tối đa chạy lại postGroup() nếu xảy ra lỗi
            times = 5
            while not post_group:
                try:
                    if times != 0:
                        # đăng vào các group được lưu trong file Post/group.txt
                        self.postGroup(content=content_text, link_group=group, file_list=file_list)
                        post_group = True
                        if i != len(groups) - 1:
                            sleep(time_between_two_posts)
                    else:
                        post_group = True
                        print('Chờ quá lâu, Không thể đăng bài')
                except Exception as e:
                    print('\r\u21BA  \033[31mCó lỗi xảy ra trong quá trình đăng bài, đang thử lại (\033[1;31m{}\033[0;31m)!\033[0m'.format(times))
                    print(e)
                    post_group = False
                    times -= 1

    def autoOnAnAccount(self, email, password, time_between_two_posts):
        # đăng nhập vào tài khoản
        login_ok = self.login(email=email, password=password)
        if login_ok  == 'ConnectionResetError':
            self.CloseDriver()
            self.driver = self.webdriver()
            self.autoOnAnAccount(
                email=email,
                password=password,
                time_between_two_posts=time_between_two_posts)
        elif login_ok:
            print('Welcom Account ', email)
            # đăng lên trang cá nhân
            self.postWall(content=content_text, file_list=file_list)
            #3----------LOOP----------- Post/group.txt
            print('==========ĐĂNG BÀI LÊN GROUP============')
            self.postToMultipleGroups(
                groups=group_list,
                time_between_two_posts=time_between_two_posts
            )
            # thực hiện xóa cookie (phiên đăng nhập) để tiến hành chuyển tài khoản đăng bài
            self.deleteCokies(email=email)
            # thực hiện nghỉ ngơi trước lần đăng nhập tiếp theo
        else:
            print('\033[31mTài khoản hoặc mật khẩu của \033[1;4;31m{}\033[0;0;31m không chính xác\033[0m'.format(email))

    def strip_emoji(self, text):
        RE_EMOJI = re.compile(u'([\U00002600-\U000027BF])|([\U0001f300-\U0001f64F])|([\U0001f680-\U0001f6FF])')
        return RE_EMOJI.sub(r'', text).strip()

    # đóng của sổ chrome hiện tại
    def CloseDriver(self):
        self.driver.quit()
    
    # tự động hóa công việc đăng bài qua các bước
    # time_between_two_login: (int) thời gian nghỉ tính bằng giây (s) giữa 2 lần chuyển tài khoản đăng bài
    # time_between_two_posts: (int) khoảng thời gian nghỉ giữa 2 lần đăng tính bằng giây(s) (nên đặt >300 nếu số lượng group đăng nhiều)
    def autoAll(self, accounts, time_between_two_login, time_between_two_posts):
        #1 hiển thị chuẩn bị đăng bài
        self.infoPost()
        #2 tạo cửa sổ chrome
        self.driver = self.webdriver()
        #3----------LOOP----------- Post/account.txt
        for account in accounts:
            # đăng nhập và đăng bài lên trang cá nhân (wall) và các nhóm (groups)
            self.autoOnAnAccount(
                email=account[EMAIL],
                password=account[PASSWORD],
                time_between_two_posts=time_between_two_posts
            )
            sleep(time_between_two_login)

test = AutoBanHang()
#accounts : danh sách các tài khoản sẽ đăng bài
#time_between_two_login: thời gian chờ giữa 2 lần login
#time_between_two_posts: thời gian chờ giữa 2 lần post bài
test.autoAll(accounts=account_list, time_between_two_login=5, time_between_two_posts=50)
test.CloseDriver()

