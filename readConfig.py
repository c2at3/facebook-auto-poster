import os
file_list = [os.getcwd() +'\\FileUpload\\'+ file for file in os.listdir('FilePost')]

content_text = []
with open('config/content.txt', 'r', encoding='utf8') as content:
    content_text = [cont for cont in content]
    content.close()
content_text = ''.join(content_text)

account_list = []
with open('config/account.txt', 'r', encoding='utf8') as accounts:
    for acc in accounts:
        account_parse = acc.split('|')
        account_parse = [a.strip() for a in account_parse]
        account_list.append(account_parse)
    accounts.close()

group_list = []
with open('config/group.txt', 'r', encoding='utf8') as groups:
    for gro in groups:
        gro = gro.strip()
        group_list.append(gro)
    groups.close()