# Facebook Auto Poster
Tự động hóa việc đăng bài viết lên trang cá nhân/story/các hội nhóm (group).

## Hướng dẫn

`FileUpload`: Thư mục này chứa các file ảnh/video sẽ được đăng lên.

`Config`: Thư mục chứa các tệp lưu cấu hình:
- `account.txt`: file này lưu thông tin các tài khoản sẽ được đăng bài.

Ví dụ:
```
email|password
username|passwd$#
tranbaquang|pasS123
quangtran|@password123@
```
- `content.txt`: file này chứa thông tin nội dung (status) sẽ được đăng lên.
- `group.txt`: file này chứa các url group sẽ đăng bài.

Ví dụ:
```
https://www.facebook.com/groups/511345833352584/
https://www.facebook.com/groups/2554344441331493/
https://www.facebook.com/groups/239365555337323/
```
`chromedriver.exe`: file này cần phải có để hoạt động và phải trùng với phiên bản chrome của máy tính chạy tool này.

Ví dụ: Máy tính chạy hệ điều hành Windown đang sử dụng Google chrome phiên bản 84.x.x.x thì chromedriver cũng phải là phiên bản 84.

Link download: https://chromedriver.chromium.org/downloads.

## Khởi chạy
### Cài thư viện
```
pip install -r requirements.txt
```
### Chạy chương trình
```
python main.py
```
or: 
```
python3 main.py
```
## Chú ý

Chương trình có thể bị lỗi theo thời gian (Do XPATH thay đổi). Lúc đó "chỉ" cần tìm và sửa đổi Xpath có trong file `main.py`, là có thể chạy bình thường.

***Chỉnh sửa lần cuối: tháng 5/2020***
