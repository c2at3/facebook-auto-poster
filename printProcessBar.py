from time import sleep


# hiển thị tiến trình
'''
    printProcessBar(toast, process)
    trong đó:
        toast: là câu in ra màn hình
        process(float): là thông số tiến trình ghi nhận
    Ví dụ: printProcessBar(toast='Đang tải lên...', 100)
    tương đương câu lệnh: print('Đang tải lên...100%)
    => Đang tải lên...100%
'''
def printProcessBar(toast, process):
    try:
        process = float(process)
        print('\r{}\033[33m{}%\033[0m'.format(toast, process), end='')
        sleep(0.1)
    except Exception:
        print('\r\033[33m{}\033[0m'.format(toast))